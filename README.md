# mistletoe-rtf

RTF output for the Python [mistletoe](https://github.com/miyuchina/mistletoe) Markdown parser

This project has moved to https://yingtongli.me/git/mistletoe-rtf/
